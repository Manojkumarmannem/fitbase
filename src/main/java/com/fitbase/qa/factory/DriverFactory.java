package com.fitbase.qa.factory;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverFactory {

	public WebDriver driver;
	
	public static ThreadLocal<WebDriver> tldriver = new ThreadLocal<>() ;
	
	public WebDriver init_driver(String browser) {
		
	try {
		if(browser.equalsIgnoreCase("chrome")) {
		WebDriverManager.chromedriver().setup();
		tldriver.set(new ChromeDriver());}
		
		else if(browser.equalsIgnoreCase("FireFox")) {
		WebDriverManager.firefoxdriver().setup();
		tldriver.set(new FirefoxDriver());}
		
		else if (browser.equalsIgnoreCase("Edge")) {
		WebDriverManager.edgedriver().setup();
		tldriver.set(new EdgeDriver());}
	}
	catch(Exception e) {
		e.getMessage();}
	
	getdriver().manage().window().maximize();
	getdriver().manage().timeouts().pageLoadTimeout(3000, TimeUnit.SECONDS);
		
		return getdriver();
	}
	
	
	public static synchronized WebDriver getdriver() {
		
		return tldriver.get();
	}
	
	
	
}
