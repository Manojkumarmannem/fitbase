package com.fitbase.qa.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {

	public WebDriver driver;

	//Constructor of the Page Class
			public LoginPage(WebDriver driver){
				this.driver=driver;	}
			
			
	By SignupandLoginButton = By.xpath("//*[@id=\'signuplogin\']/a");
	By Username 			= By.xpath("//*[@id=\'username\']");
	By Password  			= By.xpath("//*[@id=\'password\']");
	By LoginwithemailButton = By.xpath("//*[@id=\'loginemail\']/span");


	public WebElement signupandLoginButton() {

		return driver.findElement(SignupandLoginButton);
	}

	public void userUsername(String username) {

		driver.findElement(Username).sendKeys(username);
	}

	public void userPassword(String password) {

		driver.findElement(Password).sendKeys(password);
	}

	public void loginwithemailButton() {

		driver.findElement(LoginwithemailButton).click();
	}


}



