package com.fitbase.qa.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

public class ConfigReader {

	Properties prop;

	public Properties initprop() throws Exception {

		prop = new Properties();	

		FileInputStream input;
		try {
			input = new FileInputStream("./src/test/resources/Config/config.properties");
			prop.load(input);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prop;


	}
}
