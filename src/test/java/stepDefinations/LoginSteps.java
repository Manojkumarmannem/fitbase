package stepDefinations;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import com.fitbase.qa.factory.DriverFactory;
import com.fitbase.qa.pageobjects.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class LoginSteps {
	
	public WebDriver driver;
	
	LoginPage login = new LoginPage(DriverFactory.getdriver());

	@Given("^user on fitbase landing page$")
	public void user_on_fitbase_landing_page() {
	DriverFactory.getdriver().get("https://qa.fitbase.com/");

	//Assert.assertTrue("Fitbase - Wellness. Anywhere. Anytime.", true);
	
	}

	@When("^user click on signup and login button$")
	public void user_click_on_signup_and_login_button() {
		
		login.signupandLoginButton().click();
	}

	@And("^user enter Username \"([^\"]*)\"$")
	public void user_enter_Username(String username){
		login.userUsername(username);
	}

	@And("^user enter Password \"([^\"]*)\"$")
	public void user_enter_Password(String password) {
		login.userPassword(password);
	}

	@Then("^click on Login with email button$")
	public void click_on_Login_with_email_button() throws Exception {
	 login.loginwithemailButton();
	 Thread.sleep(5000);
	}

	@Then("^after login verify page title$")
	public void after_login_verify_page_title() {
	  
	//	Assert.assertEquals("Dashboard", driver.getTitle());

	}
}
