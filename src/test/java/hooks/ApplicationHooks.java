package hooks;

import java.util.Properties;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.fitbase.qa.factory.DriverFactory;
import com.fitbase.qa.utils.ConfigReader;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class ApplicationHooks {

	public WebDriver driver;	
	Properties prop;
	DriverFactory driverfactory;
	ConfigReader configreader;

	@Before(order = 0)
	public void getProperty() throws Exception {
		
		configreader = new ConfigReader();
		prop = configreader.initprop();
	}
	
	@Before(order = 1)
	public void launchBrowser() {
	
	String browsername = prop.getProperty("browser");
		driverfactory = new DriverFactory();
		driver =driverfactory.init_driver(browsername);
	}
	
	@After(order = 1)
	public void teardown(Scenario scenario) {
		
		String sceenshot = scenario.getName().replaceAll("", "_");
		byte[] source=  ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
		scenario.attach(source, "image/png", sceenshot);
}
	
//	@After(order=0)
//	public void quit() {
//		driver.quit();
//	}
	
	
}
